<%-- 
    Document   : syncinfo
    Created on : Nov 29, 2008, 6:55:39 PM
    Author     : matt
--%>
<%@ include file="head.jsp" %>
<div id="content">
<h1>Syncing your device with this Funambol server</h1>
<p>You can use any <a href="http://en.wikipedia.org/wiki/SyncML">SyncML</a>
client software to sync with this server.</p>
<p>Use the following settings:</p>
<p>
    <table><thead></thead>
        <tr><td>SyncML Server</td>
        <td>http://<%= request.getLocalName()%>:<%= request.getLocalPort()%>/funambol/ds</td></tr>
        <tr><td>Username and Password</td><td>Same as your groupware server</td></tr>
        <tr><td>Calendar sync source</td><td><em>event</em></td></tr>
        <tr><td>Task sync source</td><td><em>task</em></td></tr>
        <tr><td>Address Book sync source</td><td><em>card</em></td></tr>
</table></p>
<h2>Notes</h2>
<h3>Data format</h3>
<p>All of the above sync sources use the vCalendar/vCard formats (text/x-vcalendar / text/vcard). 
<b>Symbian OS</b> based devices, such as <em>Nokia S60</em> and <em>Symbian UIQ</em> should use <em>groupdav-s60-v</em>
as the combined calendar/task sync source.</p>
<h3>Mail</h3>
<p>If you are syncing mail, use the <em>mail</em> sync source</p>

</div>
<%@ include file="footer.jsp" %>