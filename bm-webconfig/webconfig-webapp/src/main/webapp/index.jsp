<%@ include file="head.jsp" %>
<div id="content">
    <% if (session.getAttribute("error") != null) {
    %>
    <p><em>Error encountered:</em><%=session.getAttribute("error")%></p>
    <% session.removeAttribute("error");
            }
            if (session.getAttribute("user") != null) {
    %>    
    <div id="main">
        <p>
            You are logged in as a <%= (isAdministrator ? "administator" : "user")%>!
        </p>
    </div>
    <% } else {
        if (session.getAttribute("notloggedin") != null) {
    %>
    <div id="warning">
        <p>Credentials wrong! Try again (<%= session.getAttribute("notloggedin")%>)</p>
    </div>
    <%     }%>
    <div id="login">
        <form action="login" method="post">
            Username: <input type="text" name="l_username"/> <br/>
            Password: <input type="password" name="l_password"/> <br/>
            <input type="radio" name="utype" id="admin" value="admin"/>
            <label for="admin">Administrator</label>
            <input type="radio" name="utype" id="user" value="user" checked="checked" />
            <label for="user">User</label><br/>
            <input type="submit" value="Submit"/>
        </form>
    </div>
    <%            }
    %>
</div>
<jsp:include page="footer.jsp"/>
