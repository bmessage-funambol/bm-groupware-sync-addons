$(document).ready(createUserSearch);
var curSearchResults = null;
var popupTemplate = null;
function createUserSearch() {
    $.getJSON("userSearch",userSearchResults);
    $("#search").bind("focus", function(event) {
        $("#search").val("");
    });
    document.getElementById("search").onkeyup = reloadUserSearchResults;
    $("#submitButton").bind("click", submitUserEdit);
    popupTemplate = $("#userpopuptemplate table").get(0);
}
function userSearchResults(data) {
    curSearchResults = data;
    $("#usrlist").empty();
    $("#results").empty();
    $.each(data, userSearchResultsItem);
}
function userSearchResultsItem(i,item) {
    var listId = "ulist_"+i;
    var dataId = "data_"+i;
    $("#usrlist").append("<div id=\""+listId+"\"><span>"+item[0]+"</span></div>");
    var pDataContainer = document.createElement("div");
    pDataContainer.setAttribute("id",dataId);
    var popup = popupTemplate.cloneNode(true);
    pDataContainer.appendChild(popup);
    var popupChildren = popup.getElementsByTagName("tr");
    var dataRow = popupChildren[(popupChildren.length-1)];
    var dataRowTds = dataRow.getElementsByTagName("td");
    for(var i=0;i<4;i++) {
      var popupCell = dataRowTds[i];
      popupCell.textContent = item[i];
    }
    document.getElementById("results").appendChild(pDataContainer);
    var dId = "#"+dataId;
    var lId = "#"+listId;
    $(lId).jHelperTip({
        trigger: "hover",
        dC: dId,
        autoClose:true,
        opacity: 1
    });
    var lSID = lId + " span";
    // add ajax loader
    $(lSID).bind("click", loadEditFormForUser);
    if (item[4] != null) {
        $(lSID).click(); // current user, load form for them
    }
}
function loadEditFormForUser(event) {
    var targetUserId = event.target.parentNode.id;
    var arrayId = parseInt(targetUserId.replace("ulist_",""));
    var userData = curSearchResults[arrayId];
    clearEditForm();
    $("#usr").append(userData[0]);
    $("#lname").attr("value", userData[1]);
    $("#fname").attr("value", userData[2]);
    $("#email").attr("value", userData[3]);
}
function clearEditForm() {
    $("#usr").empty();
    $("#status").empty();
    document.getElementById("fname").value = "";
    document.getElementById("lname").value = "";
    document.getElementById("email").value = "";
    document.getElementById("pass1").value = "";
    document.getElementById("pass2").value = "";
}
function reloadUserSearchResults(event) {
    $("#usrlist").empty();
    $("#usrlist").append("<div>Loading results, please wait</div>");
    $.getJSON("userSearch",{name: $("#search").val()},userSearchResults);
}
function submitUserEdit(event) {
    var user = document.getElementById("usr").textContent;
    // Check if pass1 and pass2 match
    var pass1content = document.getElementById("pass1").value;
    var pass2content = document.getElementById("pass2").value;
    if (pass1content != pass2content) {
        alert("The two password fields do not match, please enter them again");
        return;
    }
    var fnamecontent = document.getElementById("fname").value;
    var lnamecontent = document.getElementById("lname").value;
    var emailcontent = document.getElementById("email").value;
    $.ajax({type: "POST",
        url: "edituser",
        data: {user: user,        
            fname: fnamecontent,
            lname: lnamecontent,
            email: emailcontent,
            pass1: pass1content},
        error: function(xhttprequest, status, errorThrown) {
            $("#status").append("<b>Error saving user data: "+xhttprequest.responseText+" </b>");
        },
        success: function(data, textStatus) {
          $("#status").append("<b>User data saved</b>");
        }});
    }
