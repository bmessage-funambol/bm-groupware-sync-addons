<%-- 
    Document   : loggers
    Created on : May 22, 2011, 2:30:44 PM
    Author     : matt
--%>
<%
        request.setAttribute("usetwopane", "true");
        request.setAttribute("customscript", "logger.js");%>
        
        
<%@ include file="head.jsp" %>
<div id="content">
    <div>
        <span for="logname">Logger name:</span> <span id="logname"></span><br/>
        <label for="inherit">Inherit settings from parent: </label> <input id="inherit" type="checkbox"/><br/>
        <label for="loglevel">Logger level</label>
        <select id="loglevel">
            <option>OFF</option>
            <option>FATAL</option>
            <option>ERROR</option>
            <option>WARN</option>
            <option>INFO</option>
            <option>DEBUG</option>
            <option>TRACE</option>
            <option>ALL</option>
        </select> <br/>
        <button type="submit" id="submitchanges">Save settings</button>
    </div>
</div>
<div id="searchpane">
    <div id="loggerlist"></div>
</div>
<%@ include file="footer.jsp" %>