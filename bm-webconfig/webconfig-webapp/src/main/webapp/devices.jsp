<%-- 
    Document   : devices
    Created on : Dec 3, 2008, 11:16:35 AM
    Author     : matt
--%>
<%
            request.setAttribute("usetwopane", "true");
            request.setAttribute("customscript", "device.js");%>
<%@ include file="head.jsp" %>
<div id="content">
    <table>
        <tr>
            <td>Device ID</td>
            
            <td id="devid"></td>
        </tr>
        
        <tr>
            <td><label for="description">Description</label></td>
            
            <td><input type="text" id="description" /></td>
        </tr>
        
        <tr>
            <td><label for="type">Type</label></td>
            
            <td><input type="text" id="type" /></td>
        </tr>
        
        <tr>
            <td><label for="timezone">Timezone</label></td>
            
            <td><input type="text" id="timezone" /></td>
        </tr>
        
        <tr>
            <td><label for="convertdate">Convert Date</label></td>
            
            <td><input type="checkbox" id="convertdate" /></td>
        </tr>
        
        <tr>
            <td><label for="msisdn">Phone number (in intl. format with leading '+')</label></td>
            
            <td><input type="text" id="msisdn" /></td>
        </tr>
        
        <tr>
            <td><label for="address">IP address (for direct
            push)</label></td>
            
            <td><input type="text" id="address" /></td>
        </tr>
        
        <tr>
            <td><label for="notifybuild">Notification
            builder</label></td>
            
            <td><input type="text" id="notifybuild" /></td>
        </tr>
        
        <tr>
            <td><label for="notifysend">Notification
            sender</label></td>
            
            <td><input type="text" id="notifysend" /></td>
        </tr>
        
        <tr>
            <td colspan="2"><button id=
                                    "submit">Submit</button><span id="status"></span></td>
        </tr>
    </table>
</div>

<div id="searchpane">
    <p><input type="text" id="search" value=
              "Click to search.." /></p>
    
    <div id="devlist"></div>
    
    <div id="results"></div>
</div>
<div id="template">
    <div id="selectnotifypopup">
        <ul class="selectnotify">
            <li number="1">Funambol TCP-based push (default)</li>
            <li number="2">SMS Notification</li>
        </ul>
    </div>
</div>
<%@ include file="footer.jsp"%>