<%-- 
    Document   : head
    Created on : Nov 27, 2008, 11:01:15 PM
    Author     : matt
--%>
<%! int currentToolbarXPos = 0;

    String addToolBarItem(String href, String name, String image) {
        StringBuffer output = new StringBuffer();
        output.append("<div class=\"toolbarImg\" style=\"");
        output.append("left: ");
        output.append(currentToolbarXPos);
        output.append("px;\"><a href=\"").append(href).append("\">");
        output.append("<img src=\"images/").append(image).append("\" alt=\"\"/>");
        output.append("</a></div>");
        output.append("<div class=\"toolbarTxt\" style=\"");
        output.append("left: ").append((currentToolbarXPos)).append("px;");
        output.append("width: 48px;");
        output.append("\"><a href=\"").append(href).append("\">");
        output.append(name);
        output.append("</a></div>");
        currentToolbarXPos = currentToolbarXPos + 58;
        return output.toString();
    }
%>
<%
            currentToolbarXPos = 3;
%>
<%@ include file="commonhead.jsp" %>
<%@page import="com.funambol.server.config.Configuration"%>
<%@page import="com.funambol.server.config.ServerConfiguration" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link rel="stylesheet" href="adminstyle.css" type="text/css"/>
    <% if (request.getAttribute("usetwopane") != null) {%>
    <link rel="stylesheet" href="adminstyle-twopane.css" type="text/css"/>
    <% }%>
    <title>Funambol DS Server web administration</title>
    <script src="jquery-1.2.6.js" type="text/javascript"></script>
    <script src="helpertip.js" type="text/javascript"></script>
    <script src="webconfig.js" type="text/javascript"></script>
    <% if (request.getAttribute("customscript") != null) {%>
    <script src="<%=request.getAttribute("customscript")%>" type="text/javascript"></script>
    <% }%>
</head>

<body>
<div id="menu">
    <%=addToolBarItem("syncinfo.jsp", "Sync Info", "user-info.png")%>
    <% if (isAdministrator) {%>
    <%=addToolBarItem("wizard.jsp", "Groupware<br/>Wizard", "settings.png")%>
    <%=addToolBarItem("usersearch.jsp", "Users", "system-users.png")%>
    <%=addToolBarItem("devices.jsp", "Devices", "phone.png")%>
    <%=addToolBarItem("principals.jsp", "Principals", "principals.png")%>
    <%=addToolBarItem("smspush.jsp","SMS Notification", "mail-unread.png")%>
    <%=addToolBarItem("loggers.jsp","Logging config","log.png")%>
    <% } else if (user != null) {%>
    <%=addToolBarItem("usersearch.jsp", "User details", "system-users.png")%>
    <%=addToolBarItem("devices.jsp", "My devices", "phone.png")%>
    <%=addToolBarItem("principals.jsp", "Principals", "principals.png")%>
    <% }%>
    <%=addToolBarItem("about.jsp", "About", "funambol.png")%>
    <%=addToolBarItem("logout", "Logout", "logout.png")%>
</div>
