<%-- 
    Document   : about
    Created on : Nov 27, 2008, 9:12:19 PM
    Author     : matt
--%>
<%@include file="head.jsp" %>
<%@page import="com.funambol.server.admin.AdminManager" %>
<%@page import="com.funambol.framework.engine.source.SyncSource" %>
<%@page import="com.funambol.framework.filter.Clause" %>
<%@page import="com.funambol.framework.filter.WhereClause"  %>
<div id="content">
    <%
            Configuration conf = Configuration.getConfiguration();
            ServerConfiguration sc = conf.getServerConfig();
    %>
    <div id="srvInfo">    
        <ul>
            <li><%= sc.getServerInfo().getMan()%></li>
            <li>OEM: <%= sc.getServerInfo().getOEM()%></li>
            <li>Version: <%= sc.getServerInfo().getSwV()%> </li>
        </ul>
</div>
<div id="copyright">
<p>Funambol and WebConfig is licensed under the <a href="http://www.fsf.org/licensing/licenses/agpl-3.0.html">AGPLv3</a></p>
<pre><![CDATA[
WebConfig - a web based administration interface for the Funambol DS Server
    Copyright (C) 2008-2011 Mathew McBride

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]>
</pre>
<p>Please contact <a href="mailto:matt@mcbridematt.dhs.org">Mathew McBride</a> with any questions
regarding this web administration interface</p>
</div>
</div>
<%@ include file="footer.jsp" %>