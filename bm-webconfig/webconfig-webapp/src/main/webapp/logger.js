$(document).ready(setupLogger);
function setupLogger() {
    $.getJSON("logger/list",loggerList);
    $("#inherit").change(inheritSettingChanged);
    $("#submitchanges").click(saveLoggerSettings);
}

function loggerList(data) {
    $.each(data, loggerItem);
    $("#loggerlist a").click(loadLoggerSettings);
}

function loggerItem(i,item) {
    var elem = $("#loggerlist").append("<a href=\"#\">"+item+"</a><br/>");
}

function loadLoggerSettings(event) {
    
    var target = event.target;
    var name = target.innerText;
    $.getJSON("logger/get/"+name, loggerSettingsLoadHandler);
    
}

function loggerSettingsLoadHandler(data) {
    var level = data.level;
    var name = data.name;
    if (name == "funambol") {
        // Root logger node, cannot change inherit
        $("#inherit").attr("disabled","disabled");
    } else {
        $("#inherit").attr("disabled","");
    }
    var inherit = data.inherit;
    if (inherit) {
        $("#inherit").attr("checked", "checked");
        $("#loglevel").attr("disabled","disabled");
    } else {
        $("#inherit").attr("checked","");
        $("#loglevel").attr("disabled","");
    }
    var usersALL = data.usersALL;
    $("#logname").html(name);
    $("#loglevel :selected").attr('selected','');
    $("#content option:contains('"+level+"')").attr('selected','selected');
}

function inheritSettingChanged(event) {
    var inherit = $("#inherit");
    var isEnabled = $("#inherit").attr("checked");
    if (isEnabled == true) {
        $("#loglevel").attr("disabled","disabled");
    } else {
        $("#loglevel").attr("disabled","");
    }
    
}

function saveLoggerSettings(event) {
    var level = $("#loglevel :selected").html();
    var name = $("#logname").html();
    var inherit = $("#inherit").attr("checked");
    
    var data = {'name': name, 'level': level, 'inherited':inherit};
    $.ajax({'type':'POST', 'url':'logger/set/'+name,'level':'level', data: data});
}