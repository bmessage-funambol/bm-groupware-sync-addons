$(document).ready(initDeviceApp);
var curSearchResults = null;
var defaultNotify = "com/funambol/server/notification/PushSender.xml";
var smsNotify = "smsnotify/bmclickatell/bmclickatell.xml";
function initDeviceApp() {
    $.getJSON("device",deviceSearchResults);
    $("#submit").bind("click", submitDeviceEdit);
    document.getElementById("search").onkeyup = reloadDeviceSearchResults;
}
function reloadDeviceSearchResults(event) {
    $("#devlist").empty();
    $("#devlist").append("<div>Searching, please wait</div>");
    $.getJSON("device", {searchParam: $("#search").val()}, deviceSearchResults);
}
function deviceSearchResults(data) {
    curSearchResults = data;
    $("#devlist").empty();
    $("#results").empty();
    $.each(data, deviceSearchResultsItem);
}
function deviceSearchResultsItem(i, item) {
    var listId = "dlist_"+i;
    var jListId = "#"+listId;
    var dataId = "data_"+i;
    var jDataId = "#"+dataId;
    var shortenedId = item[0].substring(0, 14)+"..";
    $("#devlist").append("<div id=\""+listId+"\"><span>"+shortenedId+"</span></div>");
    var dataString = "<table id=\""+dataId+"\"><thead><tr><td>Device ID</td><td>MSISDN</td><td>IP address</td></tr></thead>";
    dataString += "<tr><td>"+item[0]+"</td><td>"+item[5]+"</td><td>"+item[6]+"</td></tr></table>";
    $("#results").append(dataString);
    $(jListId).jHelperTip({
        trigger: "hover",
	dC: jDataId,
	autoClose: true});
    $(jListId).bind("click", loadEditFormForDevice);
}
function loadEditFormForDevice(event) {
    clearEditForm();
    var targetUserId = event.target.parentNode.id;
    targetUserId = targetUserId.replace("dlist_","");
    var i = parseInt(targetUserId);
    var userData = curSearchResults[i];
    document.getElementById("devid").textContent = userData[0];
    $("#description").attr("value", userData[1]);
    $("#type").attr("value", userData[2]);
    $("#timezone").attr("value", userData[3]);
    if (userData[4] == "CONVERT_DATE") {
        $("#convertdate").attr("checked","checked");
    }
    $("#msisdn").attr("value", userData[5]);
    $("#address").attr("value", userData[6]);
    $("#notifybuild").attr("value",userData[7]);
    $("#notifysend").attr("value", userData[8]);
    $("#notifysend").jHelperTip({
        trigger: "click",
        dC: "#selectnotifypopup",
        autoClose: true
    });
    $(".selectnotify li").bind("click", handleNotifySelect);
}
function handleNotifySelect(event) {
    var target = event.target;
    var selection = target.getAttribute("number");
    if (selection == "1") {
        document.getElementById("notifysend").value = defaultNotify;
    } else if (selection == "2") {
        document.getElementById("notifysend").value = smsNotify;
    }
}
function clearEditForm() {
    $("#devid").empty();
    $("#status").empty();
    document.getElementById("description").value = "";
    document.getElementById("type").value = "";
    document.getElementById("timezone").value = "";
    document.getElementById("msisdn").value = "";
    document.getElementById("address").value = "";
    document.getElementById("notifysend").value = "";
    document.getElementById("notifybuild").value = "";
    document.getElementById("convertdate").checked = "";
}
function submitDeviceEdit() {
    var device = document.getElementById("devid").textContent;
    var descriptionValue = document.getElementById("description").value;
    var typeValue = document.getElementById("type").value;
    var timezoneValue = document.getElementById("timezone").value;
    var convertDateValue = (document.getElementById("convertdate").checked == "checked");
    var msisdnValue = document.getElementById("msisdn").value;
    var addressValue = document.getElementById("address").value;
    var notifybuildValue = document.getElementById("notifybuild").value;
    var notifysendValue = document.getElementById("notifysend").value;
    msisdnValue = msisdnValue.replace("-","");
    msisdnValue = msisdnValue.replace(" ","");
    var postData = {
        device: device,
        description: descriptionValue,
        type: typeValue,
        timezone: timezoneValue,
        convert: convertDateValue,
        msisdn: msisdnValue,
        address: addressValue,
        notifybuild: notifybuildValue,
        notifysend: notifysendValue  };
    $.ajax({type: "POST",
        url: "device",
        data: postData,
        error: function(xhttprequest, status, exception) {
            $("#status").append("<b>Error:</b> "+xhttprequest.responseText);
        }, 
        success: function(data, textStatus) {
            $("#status").append("Device information saved");
        }
    });
}