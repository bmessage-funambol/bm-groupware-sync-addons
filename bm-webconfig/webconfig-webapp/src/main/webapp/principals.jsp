<%-- 
    Document   : principals
    Created on : Dec 4, 2008, 10:50:45 AM
    Author     : matt
--%>
<%@page import="net.bionicmessage.funambol.configuration.SyncSourceUtils" %>
<%
        request.setAttribute("usetwopane", "true");
        request.setAttribute("customscript", "principal.js");%>
<%@ include file="head.jsp" %>
<div id="content">
    <table id="syncs">
        <thead>
            <tr>
            <td>Sync Source</td><td>Type</td><td>Status</td><td>Start</td><td>End</td><td>Reset</td></tr>
        </thead>
        <tbody id="syncbody">
            
        </tbody>
    </table>
    <% if (isAdministrator) {%>
    <div id="notify">
        <fieldset>
            <legend>Notification</legend>
            <p>To test push notification for this principal, select a sync source and content type below</p>
            <% String[] syncSourceURIs = SyncSourceUtils.getSyncSourceURIs(); %>
            <p> <label for="syncSourceSelect">Sync Source</label>
                <select id="syncSourceSelect">
                    <%
                    for (int i = 0; i < syncSourceURIs.length; i++) {
                    %>
                    <option><%=syncSourceURIs[i]%></option>
                    <%     }
                    %>
                </select> 
                <label for="contentTypeSelect">Content Type</label>
                <input type="text" id="contentTypeSelect"/>
                <button id="notificationTest">Test notification</button>
            </p>
            <p id="notificationFeedback">
            </p>
        </fieldset>
    </div>
    <% }%>
</div>

<div id="searchpane">
    <p><input type="text" id="search" value=
              "Search by user.." /></p>
    
    <div id="plist"></div>
    
    <div id="results"></div>
</div>
<div id="template">
    <div id="contenttypepopup">
        <ul class="selectctype">
            <li>vCalendar 1.0</li>
            <li>vCard</li>
            <li>iCalendar 2.0</li>
            <li>SIF XML Event</li>
            <li>SIF XML Task</li>
            <li>SIF XML Contact</li>
            <li>Email Folder</li>
            <li>Email Message</li>
            <li>Plain text</li>
        </ul>
    </div>
    <div id="notificationprogress">
        <p><img src="ajax-loader.gif" alt="loading animation"/> Sending notification</p>
    </div>
</div>
<%@ include file="footer.jsp"%>