<%-- 
    Document   : user
    Created on : Nov 29, 2008, 8:52:39 PM
    Author     : matt
--%>
<%
            if (session.getAttribute("isadministrator") != null) {
                request.setAttribute("usetwopane", "true");
            }
            request.setAttribute("customscript", "user.js");%>
<%@ include file="head.jsp" %>
<%@ page import="com.funambol.server.admin.UserManager" %>
<%@ page import="com.funambol.framework.filter.*" %>
<%@ page import="com.funambol.framework.server.*" %>
<div id="content">
    <table>
        <tr><td>User</td><td id="usr"></td></tr>
        <tr>
            <td>
                <label for="fname">First Name</label>
            </td>
            <td>
                <input type="text" id="fname"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="lname">Last Name</label>
            </td>
            <td>
                <input type="text" id="lname"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">Email</label>
            </td>
            <td>
                <input type="text" id="email"/>
            </td>
        </tr>
        <tr>
            <td><label for="pass1">New password</label><br/>
                <em>Changing Funambol passwords only affects administrators!</em>
            </td>
            <td>
                <input type="password" id="pass1" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="pass2">New password<br/><small>verify</small></label>
            </td>
            <td>
                <input type="password" id="pass2"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <button id="submitButton">Submit edit</button><span id="status"></span>
            </td>
        </tr>
    </table>
</div>
<div id="searchpane">
    <p><input type="text" id="search" value="Click to search.."/></p>
    <div id="usrlist">
        
    </div>
    <div id="results"></div>
</div>
<div id="template">
    <div id="userpopuptemplate">
        <table>
            <thead>
                <tr>
                    <td>Username</td>
                    <td>Last</td>
                    <td>First</td>
                    <td>Email address</td>
                </tr>
            </thead>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<%@ include file="footer.jsp" %>