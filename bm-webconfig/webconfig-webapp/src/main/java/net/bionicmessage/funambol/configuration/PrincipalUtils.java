/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.configuration;

import com.funambol.framework.core.AlertCode;
import com.funambol.framework.filter.AllClause;
import com.funambol.framework.filter.Clause;
import com.funambol.framework.filter.LogicalClause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.security.Sync4jPrincipal;
import com.funambol.framework.server.LastTimestamp;
import com.funambol.framework.server.store.PersistentStore;
import com.funambol.framework.server.store.PersistentStoreException;
import com.funambol.framework.tools.beans.BeanException;
import com.funambol.server.config.Configuration;
import com.funambol.server.store.LastTimestampPersistentStore;

/**
 *
 * @author matt
 */
public class PrincipalUtils {

    public static Sync4jPrincipal getPrincipalForId(String id) throws PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        PersistentStore ps = c.getStore();
        String[] values = {id};
        WhereClause wc = new WhereClause("id", values, WhereClause.OPT_EQ, false);
        Sync4jPrincipal template = new Sync4jPrincipal();
        Sync4jPrincipal[] results = (Sync4jPrincipal[]) ps.read(template, wc);
        if (results.length == 1) {
            return results[0];
        }
        return null;
    }

    public static LastTimestamp[] getSyncsForPrincipal(String id) throws BeanException, PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        String[] values = {id};
        WhereClause wc = new WhereClause("principal", values, WhereClause.OPT_EQ, false);
        LastTimestampPersistentStore ps =
                (LastTimestampPersistentStore) c.getBeanInstanceByName("com/funambol/server/store/LastTimestampPersistentStore.xml");
        LastTimestamp[] results =
                (LastTimestamp[]) ps.read(new LastTimestamp(), wc);
        return results;
    }

    public static void deletePrincipalSync(String priId, String source) throws BeanException, PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        // Set up two where caluses
        LastTimestamp template = new LastTimestamp(Long.parseLong(priId), source);
        LastTimestampPersistentStore ps =
                (LastTimestampPersistentStore) c.getBeanInstanceByName("com/funambol/server/store/LastTimestampPersistentStore.xml");
        ps.delete(template);

    }

    public static Sync4jPrincipal[] principalsForUser(String user) throws PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        PersistentStore ps = c.getStore();
        String[] values = {user};
        WhereClause wc = new WhereClause("username", values, WhereClause.OPT_EQ, false);
        Sync4jPrincipal template = new Sync4jPrincipal();
        return (Sync4jPrincipal[]) ps.read(template, wc);
    }

    public static Sync4jPrincipal[] getAllPrincipals() throws PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        Clause clause = new AllClause();
        PersistentStore ps = c.getStore();
        Sync4jPrincipal searchObject = new Sync4jPrincipal(-1, null, null);
        return (Sync4jPrincipal[]) ps.read(searchObject, clause);
    }

    public static String[] allowedDevicesForUser(String user) throws PersistentStoreException {
        Sync4jPrincipal[] principals = principalsForUser(user);
        String[] result = new String[principals.length];
        for (int i = 0; i < principals.length; i++) {
            Sync4jPrincipal pr = principals[i];
            result[i] = pr.getDeviceId();
        }
        return result;
    }

    public static boolean isUserAllowedToModifyDevice(String deviceId, String user) throws PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        PersistentStore ps = c.getStore();
        String[] deviceValue = {deviceId};
        String[] userValue = {user};
        WhereClause[] clauses = new WhereClause[2];
        clauses[0] = new WhereClause("device", deviceValue, WhereClause.OPT_EQ, false);
        clauses[1] = new WhereClause("username", userValue, WhereClause.OPT_EQ, false);
        LogicalClause lc = new LogicalClause(LogicalClause.OPT_AND, clauses);
        Sync4jPrincipal template = new Sync4jPrincipal();
        Sync4jPrincipal[] result = (Sync4jPrincipal[]) ps.read(template, lc);
        if (result.length == 1) {
            return true;
        }
        return false;
    }

    public static boolean isUserAllowedToModifyPrincipal(String principalId, String user) throws PersistentStoreException {
        Configuration c = Configuration.getConfiguration();
        PersistentStore ps = c.getStore();
        String[] idValue = {principalId};
        String[] userValue = {user};
        WhereClause[] clauses = new WhereClause[2];
        clauses[0] = new WhereClause("id", idValue, WhereClause.OPT_EQ, false);
        clauses[1] = new WhereClause("username", userValue, WhereClause.OPT_EQ, false);
        LogicalClause lc = new LogicalClause(LogicalClause.OPT_AND, clauses);
        Sync4jPrincipal template = new Sync4jPrincipal();
        Sync4jPrincipal[] result = (Sync4jPrincipal[]) ps.read(template, lc);
        if (result.length == 1) {
            return true;
        }
        return false;
    }

    public static String typeStringForSyncCode(int code) {
        switch (code) {
            case AlertCode.TWO_WAY:
                return "TWO_WAY";
            case AlertCode.SLOW:
                return "SLOW";
            case AlertCode.ONE_WAY_FROM_CLIENT:
                return "ONE WAY FROM CLIENT";
            case AlertCode.ONE_WAY_FROM_SERVER:
                return "ONE WAY FROM SERVER";
        }
        return Integer.toString(code);
    }
}
