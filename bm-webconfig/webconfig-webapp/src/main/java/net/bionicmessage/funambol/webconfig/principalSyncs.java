/*
 *   WebConfig - a web administration interface for the Funambol DS Server.
 *   Copyright (C) 2008 Mathew McBride
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.server.LastTimestamp;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.PrincipalUtils;
import java.text.DateFormat;
import java.util.Date;
import org.apache.log4j.helpers.ISO8601DateFormat;

/**
 *
 * @author matt
 */
public class principalSyncs extends HttpServlet {

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        String curUser = (String) session.getAttribute("user");
        try {
            String id = request.getParameter("id");
            if (!isAdministrator &&
                    !PrincipalUtils.isUserAllowedToModifyPrincipal(id, curUser)) {
                // check we have authority to operate on this id
                throw new IllegalArgumentException("You are not authorized to edit this principal");
            }
            LastTimestamp[] lt = PrincipalUtils.getSyncsForPrincipal(id);
            String[][] retData = new String[lt.length][];
            DateFormat df = new ISO8601DateFormat();
            for (int i = 0; i < lt.length; i++) {
                LastTimestamp l = lt[i];
                String[] lData = {
                    l.database,
                    PrincipalUtils.typeStringForSyncCode(l.syncType),
                    Integer.toString(l.status),
                    df.format(new Date(l.start)),
                    df.format(new Date(l.end))
                };
                retData[i] = lData;
            }
            Gson gson = new Gson();
            out.println(gson.toJson(retData));
        } catch (Exception e) {
            response.setStatus(500);
            out.println("Error: " + e.getMessage());
            e.printStackTrace(out);
        } finally {
            out.close();
        }
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=utf8");
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        String curUser = (String) session.getAttribute("user");
        String syncSource = request.getParameter("syncsource");
        String principalId = request.getParameter("principalId");
        try {
            if (!isAdministrator &&
                    !PrincipalUtils.isUserAllowedToModifyPrincipal(principalId, curUser)) {
                // check we have authority to operate on this id
                throw new IllegalArgumentException("You are not authorized to edit this principal");
            }
            PrincipalUtils.deletePrincipalSync(principalId, syncSource);
            out.println("Success");
        } catch (Exception e) {
            response.setStatus(500);
            out.println(e.getMessage());
        } finally {
            out.close();
        }


        out.println("Method not implemented");
    }

    /** 
     * Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
