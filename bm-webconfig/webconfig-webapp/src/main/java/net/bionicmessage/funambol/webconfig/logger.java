/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bionicmessage.funambol.webconfig;

import com.funambol.framework.config.LoggerConfiguration;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.bionicmessage.funambol.configuration.LoggerUtils;

/**
 *
 * @author matt
 */
public class logger extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        boolean isAdministrator = (session.getAttribute("isadministrator") != null);
        if (!isAdministrator)
            throw new ServletException("Not authorized");
        response.setContentType("application/json;charset=UTF-8");
        String pathInfo = request.getPathInfo();
        String[] pathComponents = null;
         
        if (pathInfo == null) {
            pathComponents = new String[]{"list"};
        } else {
            pathComponents = pathInfo.substring(1).split("/");
        }
        PrintWriter out = response.getWriter();
        try {
            if (pathComponents.length == 0 || pathComponents[0].equals("list")) {
                String[] loggers = LoggerUtils.getLoggers();
                Gson gson = new Gson();
                out.println(gson.toJson(loggers));
            } else if (pathComponents[0].equals("get")) {
                String logger = pathComponents[1];
                LoggerConfiguration lc = LoggerUtils.getLogger(logger);
                HashMap map = new HashMap();
                map.put("name", lc.getName());
                map.put("inherit",lc.isInherit());
                map.put("level",lc.getLevel());
                map.put("usersALL",lc.getUsersWithLevelALL());
                Gson gson = new Gson();
                out.println(gson.toJson(map));
            } else if (pathComponents[0].equals("set")) {
                String logger = pathComponents[1];
                String newSetting = request.getParameter("level");
                LoggerUtils.setLoggerLevel(logger, newSetting);
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
