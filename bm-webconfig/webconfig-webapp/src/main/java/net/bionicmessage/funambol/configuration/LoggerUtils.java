/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bionicmessage.funambol.configuration;

import com.funambol.framework.config.LoggerConfiguration;
import com.funambol.server.config.Configuration;
import java.util.ArrayList;
/**
 *
 * @author matt
 */
public class LoggerUtils {
    
    
    public static String[] getLoggers() {
        Configuration c = Configuration.getConfiguration();
        LoggerConfiguration[] loggers = c.getLoggers();
        String[] logNames = new String[loggers.length];
        for (int i = 0; i < loggers.length; i++) {
            LoggerConfiguration loggerConfiguration = loggers[i];
            logNames[i] = loggerConfiguration.getName();
        }
        java.util.Arrays.sort(logNames);
        return logNames;
        
    }
    public static void setLoggerLevel(String name, String level) {
        LoggerConfiguration lc = getLogger(name);
        if (lc == null)
            return;
        lc.setLevel(level);
        Configuration c = Configuration.getConfiguration();
        c.setLoggerConfiguration(lc);
    }
    
    public static String getLoggerLevel(String name) {
        LoggerConfiguration lc = getLogger(name);
        if (lc == null) 
            return "";
        return lc.getLevel();
    }
    
    public static LoggerConfiguration getLogger(String name) {
        Configuration c = Configuration.getConfiguration();
        LoggerConfiguration[] loggers = c.getLoggers();
        for (int i = 0; i < loggers.length; i++) {
            LoggerConfiguration loggerConfiguration = loggers[i];
            if (name.equals(loggerConfiguration.getName())) 
                return loggerConfiguration;
        }
        return null;
    }
    
}
