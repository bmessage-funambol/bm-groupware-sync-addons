/*
 * Post configuration utility for Funambol/Groupware sync server
 * Copyright (C) 2008 Mathew McBride
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package net.bionicmessage.funambol.bundle;

import com.funambol.admin.util.WSTools;
import java.io.InputStream;
import java.util.*;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSourceInfo;
import com.funambol.framework.filter.Clause;
import com.funambol.framework.filter.WhereClause;
import com.funambol.framework.server.Sync4jSource;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import net.bionicmessage.funambol.BVersion;
import net.bionicmessage.funambol.framework.Constants;
import net.bionicmessage.funambol.groupdav.calendar.CalendarSyncSource;
import net.bionicmessage.funambol.groupdav.contacts.ContactSyncSource;

public class SetupUtils {

    /** Load a property file from the JAR with the default paths for the server 
     * 
     * @param srvType Server type (citadel/opengroupware/egroupware from 1-3 respectively)
     * @return Filled Properties instance.
     * @throws java.lang.Exception
     */
    public static Properties getDefaultURLsForServer(int srvType) throws Exception {
        String server = "citadel"; //failsafe default
        if (srvType == 2) {
            server = "egroupware";
        } else if (srvType == 3) {
            server = "opengroupware";
        }
        String resourcePath = "/net/bionicmessage/funambol/bundle/"+server+".properties";
        InputStream is = SetupUtils.class.getResourceAsStream(resourcePath);
        Properties po = new Properties();
        po.load(is);
        return po;
    }

    /** Serialize an object 
     * @param obj Object to serialize
     * @return Object serialized to XML
     */
    public static String serializeObject(Object obj) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLEncoder e = new XMLEncoder(bos);
        e.writeObject(obj);
        e.close();
        return bos.toString();
    }

    
    public static String setupCalendarSyncSource(String serverURL,
            String path,
            String type,
            String uri,
            String stDirRoot) throws Exception {
        CalendarSyncSource css = new CalendarSyncSource();
        css.setSourceURI(uri);
        css.setName(uri);
        Properties syncSourceProperties = new Properties();

        // ctypes and syncsourceinfo
        ContentType ctype = new ContentType();
        ctype.setType(type);
        ctype.setVersion("1.0");
        ContentType[] ctypes = new ContentType[]{ctype};
        SyncSourceInfo ssInfo = new SyncSourceInfo(ctypes, 0);
        css.setInfo(ssInfo);

        // basic properties
        syncSourceProperties.setProperty(Constants.SERVER_HOST, serverURL);

        syncSourceProperties.setProperty(Constants.SOURCE_LOCATION_BASE + "default", path);
        syncSourceProperties.setProperty(Constants.SINGLE_LOG, "");

        String storePath = stDirRoot + File.separator + uri;
        syncSourceProperties.setProperty(Constants.STOREDIR_PATH, storePath);
        css.setConnectorProperties(syncSourceProperties);
        return serializeObject(css);
    }

    public static String setupContactSyncSource(String serverURL,
            String path,
            String type,
            String uri,
            String stDirRoot) throws Exception {
        ContactSyncSource css = new ContactSyncSource();
        css.setSourceURI(uri);
        css.setName(uri);
        Properties syncSourceProperties = new Properties();

        // ctypes and syncsourceinfo
        ContentType ctype = new ContentType();
        ctype.setType(type);
        if (type.equals("text/x-vcard")) {
            ctype.setVersion("2.1");
        } else {
            ctype.setVersion("1.0");
        }
        ContentType[] ctypes = new ContentType[]{ctype};
        SyncSourceInfo ssInfo = new SyncSourceInfo(ctypes, 0);
        css.setInfo(ssInfo);

        // basic properties
        syncSourceProperties.setProperty(Constants.SERVER_HOST, serverURL);

        syncSourceProperties.setProperty(Constants.SOURCE_LOCATION_BASE + "default", path);
        syncSourceProperties.setProperty(Constants.SINGLE_LOG, "");

        String storePath = stDirRoot + File.separator + uri;
        syncSourceProperties.setProperty(Constants.STOREDIR_PATH, storePath);
        css.setConnectorProperties(syncSourceProperties);
        return serializeObject(css);
    }
    
     public static String setupS60CalendarSyncSource(String serverURL,
            String calPath,
            String taskPath,
            String stDirRoot) throws Exception {
        CalendarSyncSource css = new CalendarSyncSource();
        css.setSourceURI("groupdav-s60-v");
        css.setName("groupdav-s60-v");
        Properties syncSourceProperties = new Properties();

        // ctypes and syncsourceinfo
        ContentType ctype = new ContentType();
        ctype.setType("text/x-vcalendar");
        ctype.setVersion("1.0");
        ContentType[] ctypes = new ContentType[]{ctype};
        SyncSourceInfo ssInfo = new SyncSourceInfo(ctypes, 0);
        css.setInfo(ssInfo);

        // basic properties
        syncSourceProperties.setProperty(Constants.SERVER_HOST, serverURL);

        syncSourceProperties.setProperty(Constants.SOURCE_LOCATION_BASE + "default", calPath);
        syncSourceProperties.setProperty(Constants.SOURCE_LOCATION_BASE + "tasks",taskPath);
        syncSourceProperties.setProperty(Constants.SINGLE_LOG, "");
        syncSourceProperties.setProperty(Constants.MIXED_MODE, "");
        
        String storePath = stDirRoot + File.separator + "groupdav-s60-v";
        syncSourceProperties.setProperty(Constants.STOREDIR_PATH, storePath);
        css.setConnectorProperties(syncSourceProperties);
        return serializeObject(css);
    }
    
    /** 
     * Does sync source already exist?
     * @param ssId Sync Source URI
     * @param fnbl WSTools instance to query with
     * @return true if sync source is defined. 
     */
    public static boolean doesSyncSourceExist(String ssId, WSTools fnbl) throws Exception {
        Clause clause = getClauseForSync4jSourceUriAndNameCheck(ssId, null);
        String[] searchArg = new String[]{SetupUtils.serializeObject(clause)};
        Sync4jSource[] sources = (Sync4jSource[]) fnbl.invoke("getSync4jSources", searchArg);
        if (sources.length > 0) {
            return true;
        }
        return false;
    }
    /** Function copied from SyncSourceController */
    private static Clause getClauseForSync4jSourceUriAndNameCheck(String uri, String name) {

        List clauses = new ArrayList();

        if (uri == null && name == null) {
            return null;
        }

        Clause uriClause = null;

        if (uri != null) {
            uriClause = new WhereClause("uri",
                    new String[]{uri},
                    WhereClause.OPT_EQ,
                    false);
            clauses.add(uriClause);
        }

        return uriClause;
    }
}
