/*
 * Post configuration utility for Funambol/Groupware sync server
 * Copyright (C) 2008 Mathew McBride
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package net.bionicmessage.funambol.bundle;

import com.funambol.admin.util.*;
import com.funambol.server.config.*;
import com.funambol.framework.filter.*;
import net.bionicmessage.funambol.httpauth.HTTPAuthenticationOfficer;
import net.bionicmessage.funambol.citadel.sync.CitadelSyncSource;
import net.bionicmessage.funambol.citadel.store.CtdlFnblConstants;

import com.funambol.framework.engine.source.ContentType;
import com.funambol.framework.engine.source.SyncSourceInfo;

import java.io.*;
import java.util.*;

import com.funambol.framework.server.*;
import com.funambol.server.inventory.PSDeviceInventory;
import java.net.URL;
import org.apache.log4j.LogManager;
import org.apache.log4j.varia.NullAppender;

public class postConfig {

    public static final String HTTPAUTHBEAN = "net/bionicmessage/funambol/httpauth/HTTPAuthenticationOfficer.xml";
    private static String adminPass = null;
    private static String WSPath = null;
    private static String adminUser = null;

    /** Main method */
    public static void main(String args[]) {
        if (args.length < 1) {
            System.out.println("No arguments specified, exiting");
            System.out.println("Operations: ");
            System.out.println("usehttpauth [server] [servertype] - Use HTTP Auth");
            System.out.println("usedefaultauth - Use the default UserProvisioningOfficer");
            System.out.println("queryofficer - Get bean for current officer");
            System.out.println("createcitadel [ctdlhost] [storeroot] - Creates a citadel sync source");
            System.out.println("createpim [server] [storeroot] [servertype] - Create PIM sync sources");
            System.out.println("doesexist [syncsourcename] - does a sync source URI exist?");
            System.out.println("setusepushnotify - set to use SMS Push for all new devices");
            System.out.println("changeadminpw - change administrator password");
            return;
        }
        String operation = args[0];
        try {
            /* Silence log4j: http://forum.springframework.org/showthread.php?t=52297 */

            System.setProperty("log4j.defaultInitOverride", "tr ue");
            LogManager.resetConfiguration();
            LogManager.getRootLogger().addAppender(new NullAppender());
            /* Load the server configuration */
            File serverPropsFile = new File("funambolserver.properties");
            WSPath = "http://localhost:8080/funambol/services/admin";
            adminUser = "admin";
            adminPass = "sa";
            if (serverPropsFile.exists()) {
                Properties alternativeConfiguration = new Properties();
                alternativeConfiguration.load(new FileInputStream(serverPropsFile));
                WSPath = alternativeConfiguration.getProperty("wspath");
                adminUser = alternativeConfiguration.getProperty("user");
                adminPass = alternativeConfiguration.getProperty("pass");
            }
            WSTools fnbl = new WSTools(WSPath, adminUser, adminPass);
            ServerConfiguration config = (ServerConfiguration) fnbl.invoke("getServerConfiguration", null);
            EngineConfiguration eConfig = config.getEngineConfiguration();
            System.out.println("Current officer: " + eConfig.getOfficer());
            if (operation.equals("usehttpauth") && args.length >= 2) {
                eConfig.setOfficer(HTTPAUTHBEAN);
                String[] setupArgs = new String[]{
                    HTTPAUTHBEAN,
                    setupAndSerializeHTTPAuth(args[1],Integer.parseInt(args[2]))
                };
                fnbl.invoke("setServerBean", setupArgs);
                fnbl.invoke("setServerConfiguration", new ServerConfiguration[]{config});
            } else if (operation.equals("usedefaultauth")) {
                eConfig.setOfficer(EngineConfiguration.DEFAULT_OFFICER);
                fnbl.invoke("setServerConfiguration", new ServerConfiguration[]{config});
            } else if (operation.equals("queryofficer")) {
                String officer = (String) fnbl.invoke("getServerBean", new String[]{eConfig.getOfficer()});
                System.out.println(officer);
            } else if (operation.equals("createcitadel") && args.length == 3) {
                if (!SetupUtils.doesSyncSourceExist("mail", fnbl)) {
                    String ss = setupCitadelSyncSource(args[1], args[2]);
                    String[] createArgs = new String[]{
                        "bmCitadel",
                        "citadel",
                        "citadelMail",
                        ss
                    };
                    fnbl.invoke("addSource", createArgs);
                } else {
                    System.out.println("Mail sync source not created (already exists)");
                }
            } else if (operation.equals("createpim") && args.length == 4) {
                setupGroupDAVSyncSources(args[1], args[2], Integer.parseInt(args[3]), fnbl);
            } else if (operation.equals("doesexist") && args.length == 2) {
                System.out.println(SetupUtils.doesSyncSourceExist(args[1], fnbl));
            } else if (operation.equals("setusepushnotify")) {
                setPushToSMS(fnbl);
            } else if (operation.equals("changeadminpw")) {
                setAdminPassword(fnbl);
            }
            // Save configuration file
            Properties pcProps = new Properties();
            pcProps.setProperty("wspath", WSPath);
            pcProps.setProperty("user", adminUser);
            pcProps.setProperty("pass", adminPass);
            pcProps.store(new PrintStream("funambolserver.properties"), "Properties for postconfig tool");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Sets up a CitadelSyncSource and serializes it for submission
     * @param server Citadel server (i.e uncensored.citadel.org)
     * @param storeRootPath Root path for stores (i.e /opt/Funambol/stores)
     */
    public static String setupCitadelSyncSource(String server, String storeRootPath)
            throws Exception {
        String host = null;
        String port = null;
        if (server.contains(":")) {
            String[] srvParts = server.split(":");
            host = srvParts[0];
            port = srvParts[1];
        } else {
            host = server;
            port = "504";
        }
        String stPath = storeRootPath + File.separator + "mail";
        CitadelSyncSource css = new CitadelSyncSource();
        Properties syncSourceProps = new Properties();
        syncSourceProps.setProperty(CtdlFnblConstants.SERVER_HOST, server);
        syncSourceProps.setProperty(CtdlFnblConstants.SERVER_PORT, port);
        syncSourceProps.setProperty(CtdlFnblConstants.ROOM_MAIL, "Mail");
        syncSourceProps.setProperty(CtdlFnblConstants.STORE_LOC, stPath);
        SyncSourceInfo ssInfo = new SyncSourceInfo();
        ContentType[] ctypes = new ContentType[]{
            new ContentType("application/vnd.omads-email+xml", "1.2"),
            new ContentType("application/vnd.omads-folder+xml", "1.2")
        };
        ssInfo.setSupportedTypes(ctypes);
        css.setInfo(ssInfo);
        css.setSyncSourceProperties(syncSourceProps);
        css.setSourceURI("mail");
        css.setName("mail");
        return SetupUtils.serializeObject(css);
    }

    /** Sets up a HTTPAuthenticationOfficer bean for the specified URL 
     * and returns the serialized bean 
     * @param server The HTTP URL to authenticate against
     */
    public static String setupAndSerializeHTTPAuth(String server, int srvType) throws Exception {
        Properties defaultPaths = SetupUtils.getDefaultURLsForServer(srvType);
        HTTPAuthenticationOfficer newOfficer = new HTTPAuthenticationOfficer();
        URL authPath= new URL(new URL(server),defaultPaths.getProperty("calendar"));
        newOfficer.setAuthenticationPath(authPath.toExternalForm());
        return SetupUtils.serializeObject(newOfficer);
    }

    public static void setPushToSMS(WSTools fnbl) throws Exception {
        String[] srvBean = new String[]{"com/funambol/server/inventory/PSDeviceInventory.xml"};
        PSDeviceInventory dI = (PSDeviceInventory) fnbl.invoke("getServerBean", srvBean);
        dI.getDevicePersistentStore().setDefaultNotificationSender("smsnotify/bmclickatell/bmclickatell.xml");
        String serialized = SetupUtils.serializeObject(dI);
        String[] save = new String[]{"com/funambol/server/inventory/PSDeviceInventory.xml", serialized};
        fnbl.invoke("setServerBean", save);
        System.out.println("Server now set to use SMS notification. You will need");
        System.out.println("to configure the SMS notification in the configuration too");
        System.out.println("as well as configure the phone number (MSISDN) for the device");
        System.out.println("after first sync");
    }

    /** Sets up the default GroupDAV sync sources
     * @param server The GroupDAV HTTP server host and port (i.e &quot;http://comalies.citadel.org:2000&quot;)
     * @param stDirRoot The root dir for stores (i.e /opt/Funambol);
     * @param srvType Server type id
     * @param fnbl WSTools instance to use 
     */
    public static void setupGroupDAVSyncSources(String server,
            String stDirRoot,
            int srvType,
            WSTools fnbl) throws Exception {
        Properties defaultPaths = SetupUtils.getDefaultURLsForServer(srvType);
        if (!SetupUtils.doesSyncSourceExist("groupdav-cal-s", fnbl)) {
            String source = SetupUtils.setupCalendarSyncSource(server,
                    defaultPaths.getProperty("calendar"),
                    "text/x-s4j-sife",
                    "groupdav-cal-s",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavCal",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-cal-v", fnbl)) {
            String source = SetupUtils.setupCalendarSyncSource(server,
                    defaultPaths.getProperty("calendar"),
                    "text/x-vcalendar",
                    "groupdav-cal-v",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavCal",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-task-s", fnbl)) {
            String source = SetupUtils.setupCalendarSyncSource(server,
                    defaultPaths.getProperty("task"),
                    "text/x-s4j-sift",
                    "groupdav-task-s",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavCal",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-task-v", fnbl)) {
            String source = SetupUtils.setupCalendarSyncSource(server,
                    defaultPaths.getProperty("task"),
                    "text/x-vcalendar",
                    "groupdav-task-v",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavCal",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-addr-v", fnbl)) {
            String source = SetupUtils.setupContactSyncSource(server,
                    defaultPaths.getProperty("address"),
                    "text/x-vcard",
                    "groupdav-addr-v",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavContact",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-addr-s", fnbl)) {
            String source = SetupUtils.setupContactSyncSource(server,
                    defaultPaths.getProperty("address"),
                    "text/x-s4j-sifc",
                    "groupdav-addr-s",
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavContact",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
        if (!SetupUtils.doesSyncSourceExist("groupdav-s60-v", fnbl)) {
            String source = SetupUtils.setupS60CalendarSyncSource(server,
                    defaultPaths.getProperty("calendar"),
                    defaultPaths.getProperty("task"),
                    stDirRoot);
            String[] createArgs = new String[]{
                "groupdav",
                "groupdav",
                "groupdavCal",
                source
            };
            fnbl.invoke("addSource", createArgs);
        }
    }

    private static void setAdminPassword(WSTools fnbl) throws Exception {
        WhereClause wc = new WhereClause("username", new String[]{"admin"}, WhereClause.OPT_EQ, false);
        String clause = SetupUtils.serializeObject(wc);
        Sync4jUser[] result = (Sync4jUser[]) fnbl.invoke("getUsers", new String[]{clause});
        Sync4jUser admin = result[0];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter a new password for the Funambol admin account");
        adminPass = br.readLine();
        admin.setPassword(adminPass);
        fnbl.invoke("setUser", new Object[]{admin});
    }
}
